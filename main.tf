resource "google_compute_instance" "default" {
  // Create a new gce instance with a dummy startup script and a debian-9 image
  name = var.application_name
  machine_type = var.machine_type
  zone = "europe-west1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = "default"
  }
}
